<?php

namespace CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="CommandeBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
    /**
     * @ORM\OneToMany(targetEntity="CommandeBundle\Entity\Categorie", mappedBy="category", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $subcat;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Category
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subcat = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add subcat
     *
     * @param \CommandeBundle\Entity\Categorie $subcat
     *
     * @return Category
     */
    public function addSubcat(\CommandeBundle\Entity\Categorie $subcat)
    {
        $this->subcat[] = $subcat;

        return $this;
    }

    /**
     * Remove subcat
     *
     * @param \CommandeBundle\Entity\Categorie $subcat
     */
    public function removeSubcat(\CommandeBundle\Entity\Categorie $subcat)
    {
        $this->subcat->removeElement($subcat);
    }

    /**
     * Get subcat
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubcat()
    {
        return $this->subcat;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
