<?php

namespace CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commandes
 *
 * @ORM\Table(name="commandes")
 * @ORM\Entity(repositoryClass="CommandeBundle\Repository\CommandesRepository")
 */
class Commandes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=255, nullable=true)
     */
    private $num;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;
    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", options={"default":"0"})
     */
    private $etat;
    /**
     * @var bool
     *
     * @ORM\Column(name="fin", type="boolean", options={"default":"0"})
     */
    private $fin;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $facture;
	 /**
     * @var string
     *
     * @ORM\Column(name="sage", type="string", length=255, nullable=true)
     */
    private $sage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\User $utilisateur
     *
     * @return Commandes
     */
    public function setUtilisateur(\UserBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\User
*/
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }


    /**
     * Set num
     *
     * @param string $num
     *
     * @return Commandes
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return string
     */
    public function getNum()
    {
        return $this->num;
    }
    public function __toString()
    {
        return $this->num;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Commandes
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set fin
     *
     * @param boolean $fin
     *
     * @return Commandes
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return boolean
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set facture
     *
     * @param \CommandeBundle\Entity\Media $facture
     *
     * @return Commandes
     */
    public function setFacture(\CommandeBundle\Entity\Media $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * Get facture
     *
     * @return \CommandeBundle\Entity\Media
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Commandes
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
	  /**
     * Set sage
     *
     * @param string $sage
     *
     * @return Commandes
     */
    public function setSage($sage)
    {
        $this->sage = $sage;

        return $this;
    }

    /**
     * Get sage
     *
     * @return string
     */
    public function getSage()
    {
        return $this->sage;
    }
}
