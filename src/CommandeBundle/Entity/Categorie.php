<?php

namespace CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="CommandeBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
    /**
     * @ORM\ManyToOne(targetEntity="CommandeBundle\Entity\Category", inversedBy="subcat")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;
    /**
     * @ORM\OneToMany(targetEntity="CommandeBundle\Entity\Produit", mappedBy="categorie", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $produit;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Remove produit
     *
     * @param \CommandeBundle\Entity\Produit $produit
     */
    public function removeProduit(\CommandeBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduit()
    {
        return $this->produit;
    }
    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Set category
     *
     * @param \CommandeBundle\Entity\Category $category
     *
     * @return Categorie
     */
    public function setCategory(\CommandeBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \CommandeBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add produit
     *
     * @param \CommandeBundle\Entity\Produit $produit
     *
     * @return Categorie
     */
    public function addProduit(\CommandeBundle\Entity\Produit $produit)
    {
        $this->produit[] = $produit;

        return $this;
    }
}
