<?php

namespace CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prixprod
 *
 * @ORM\Table(name="prixprod")
 * @ORM\Entity(repositoryClass="CommandeBundle\Repository\PrixprodRepository")
 */
class Prixprod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;


    /**
     * @var int
     *
     * @ORM\Column(name="marge", type="integer")
     */
    private $marge;

    /**
     * @var int
     *
     * @ORM\Column(name="coef", type="integer")
     */
    private $coef;


    /**
     * @var float
     *
     * @ORM\Column(name="prixttc", type="float")
     */
    private $prixttc;

    /**
     * @ORM\ManyToOne(targetEntity="CommandeBundle\Entity\Produit", inversedBy="prod")
     * @ORM\JoinColumn(nullable=true)
     */
    private $produit;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="prod")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Prixprod
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set produit
     *
     * @param \CommandeBundle\Entity\Produit $produit
     *
     * @return Prixprod
     */
    public function setProduit(\CommandeBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \CommandeBundle\Entity\Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\User $utilisateur
     *
     * @return Prixprod
     */
    public function setUtilisateur(\UserBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\User
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set marge
     *
     * @param integer $marge
     *
     * @return Prixprod
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return integer
     */
    public function getMarge()
    {
        return $this->marge;
    }

    /**
     * Set coef
     *
     * @param integer $coef
     *
     * @return Prixprod
     */
    public function setCoef($coef)
    {
        $this->coef = $coef;

        return $this;
    }

    /**
     * Get coef
     *
     * @return integer
     */
    public function getCoef()
    {
        return $this->coef;
    }

    /**
     * Set prixttc
     *
     * @param float $prixttc
     *
     * @return Prixprod
     */
    public function setPrixttc($prixttc)
    {
        $this->prixttc = $prixttc;

        return $this;
    }

    /**
     * Get prixttc
     *
     * @return float
     */
    public function getPrixttc()
    {
        return $this->prixttc;
    }


}
