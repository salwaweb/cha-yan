<?php

namespace CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="CommandeBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255)
     */
    private $ref;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="text")
     */
    private $designation;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;
    /**
     * @var string
     *
     * @ORM\Column(name="gramme", type="string", length=255,nullable=true)
     */
    private $gramme;

    /**
     * @ORM\ManyToOne(targetEntity="CommandeBundle\Entity\Categorie", inversedBy="produit")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="CommandeBundle\Entity\Commandes", mappedBy="produit", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $commmande;
    /**
     * @ORM\OneToMany(targetEntity="CommandeBundle\Entity\Prixprod", mappedBy="produit", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $prod;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Produit
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commmande = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add commmande
     *
     * @param \CommandeBundle\Entity\Commandes $commmande
     *
     * @return Produit
     */
    public function addCommmande(\CommandeBundle\Entity\Commandes $commmande)
    {
        $this->commmande[] = $commmande;

        return $this;
    }

    /**
     * Remove commmande
     *
     * @param \CommandeBundle\Entity\Commandes $commmande
     */
    public function removeCommmande(\CommandeBundle\Entity\Commandes $commmande)
    {
        $this->commmande->removeElement($commmande);
    }

    /**
     * Get commmande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommmande()
    {
        return $this->commmande;
    }



    


    /**
     * Set image
     *
     * @param \CommandeBundle\Entity\Media $image
     *
     * @return Produit
     */
    public function setImage(\CommandeBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \CommandeBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return Produit
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer
     */
    public function getQte()
    {
        return $this->qte;
    }

    

  

    /**
     * Add prod
     *
     * @param \CommandeBundle\Entity\Prixprod $prod
     *
     * @return Produit
     */
    public function addProd(\CommandeBundle\Entity\Prixprod $prod)
    {
        $this->prod[] = $prod;

        return $this;
    }

    /**
     * Remove prod
     *
     * @param \CommandeBundle\Entity\Prixprod $prod
     */
    public function removeProd(\CommandeBundle\Entity\Prixprod $prod)
    {
        $this->prod->removeElement($prod);
    }

    /**
     * Get prod
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProd()
    {
        return $this->prod;
    }

    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Set gramme
     *
     * @param string $gramme
     *
     * @return Produit
     */
    public function setGramme($gramme)
    {
        $this->gramme = $gramme;

        return $this;
    }

    /**
     * Get gramme
     *
     * @return string
     */
    public function getGramme()
    {
        return $this->gramme;
    }

    /**
     * Set ref
     *
     * @param string $ref
     *
     * @return Produit
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set categorie
     *
     * @param \CommandeBundle\Entity\Categorie $categorie
     *
     * @return Produit
     */
    public function setCategorie(\CommandeBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \CommandeBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
