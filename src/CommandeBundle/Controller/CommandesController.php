<?php

namespace CommandeBundle\Controller;

use CommandeBundle\Entity\Commandes;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// Include the Response and ResponseHeaderBag
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Commande controller.
 *
 */
class CommandesController extends Controller
{
	/** @var EngineInterface */
		private $templating;
		
    /**
     * Lists all commande entities.
     *
     */
    public function exelAction(Commandes $commande)
    {
        $em = $this->getDoctrine()->getManager();
        $comd= $em->getRepository('CommandeBundle:Commandes')->findOneBy(array('id' =>$commande));
        $num=$comd->getNum();
		$sage=$comd->getSage();
        $date=$comd->getCreatedAt()->format('ymd');
		//dd($date);
        $cmd=$comd->getId();
        $user=$comd->getUtilisateur();
        $client= $em->getRepository('UserBundle:User')->findOneBy(array('id' =>$user));
        $numro=$client->getNumero();
		$name=$client->getUsername();
        //dd($name);
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
        // ask the service for a excel object
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("liuggio")
            ->setLastModifiedBy("Giulio De Donato")
            ->setTitle("Office 2005 XLSX Test Document")
            ->setSubject("Office 2005 XLSX Test Document")
            ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
            ->setKeywords("office 2005 openxml php")
            ->setCategory("Test result file");
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Sage')
            ->setCellValue('B1', 'Date cmd')
            ->setCellValue('C1', 'Num cmd')
            ->setCellValue('D1', 'Num client')
            ->setCellValue('E1', 'Référence')
            ->setCellValue('F1', 'Article')
            ->setCellValue('G1', 'Prix unitaire')
            ->setCellValue('H1', 'Quantité')
            ->setCellValue('I1', 'Prix de base')
            ->setCellValue('J1', '%')
            ->setCellValue('K1', 'Total');
        $sheet
        =$phpExcelObject->getActiveSheet()->setTitle('Commande');

        $lignes = 2;
        $qt=0;
        $ttc=0;
        foreach ($items as $it) {
            if ($it->getOrder() == $cmd) {
                $itt=$it->getProduct();
                foreach ($products as $prd) {
                    if ($prd->getId() == $itt) {
                        $p=$prd->getProduit();
                        $prod=$em->getRepository('CommandeBundle:Produit')->findOneBy(array('id' =>$p));
                        $sheet->setCellValue('A'. $lignes, $sage);
                        $sheet->setCellValue('B'. $lignes, $date);
                        $sheet->setCellValue('C'. $lignes, $num);
                        $sheet->setCellValue('D'. $lignes, $numro);
                        $sheet->setCellValue('E' . $lignes, $prod->getRef());
                        $sheet->setCellValue('F' . $lignes, $prod->getNom());
                        $sheet->setCellValue('G' . $lignes, $it->getPrice());
                        $sheet->setCellValue('H' . $lignes, $it->getQty());


                    }
                }
                $sheet->setCellValue('I' . $lignes, $it->getTotal());
                $qt=$it->getQty()+$qt;
                $ttc=$it->getTotal()+$ttc;
				
				
                $lignes++;
            }
			if($ttc<400){
			$tt=$ttc+18;}
			else{
				$tt=$ttc;
			}
			if($ttc<400){
				
					$sheet->setCellValue('A'. $lignes, $sage);
                        $sheet->setCellValue('B'. $lignes, $date);
                        $sheet->setCellValue('C'. $lignes, $num);
                        $sheet->setCellValue('D'. $lignes, $numro);
						$sheet->setCellValue('E' . $lignes, 'P');
                        $sheet->setCellValue('F' . $lignes, 'Port');
                        $sheet->setCellValue('G' . $lignes, '18');
                        $sheet->setCellValue('H' . $lignes, '1');
						$sheet->setCellValue('I' . $lignes, $tt);
						$sheet->setCellValue('J' . $lignes, '0%');
                       
				}
        }

        
        if ( $qt>5 && $qt<10 ) {
            $sheet->setCellValue('J2', '3%');
            $remise = $tt * 3 / 100;
			$net = $tt - $remise;
            $sheet->setCellValue('K2',  $net);
        }
        elseif ( $qt>9 ) {
            $sheet->setCellValue('J2', '5%');
            $remise = $tt* 5 / 100;
			 $net = $tt - $remise;
            $sheet->setCellValue('K2',  $net);


        }
        else{
            $sheet->setCellValue('J2', '0%');

            $sheet->setCellValue('K2', $tt);


        }


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
		 $filename = ''.$name.$date.'COMMANDE.xlsx';
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
          $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
		
		
		
		
        return $response;

    }
	
	/**
     * Lists all commande entities.
     *
     */
    public function textAction(Commandes $commande)
    {
		
		$em = $this->getDoctrine()->getManager();
        $comd= $em->getRepository('CommandeBundle:Commandes')->findOneBy(array('id' =>$commande));
        $num=$comd->getNum();
		$sage=$comd->getSage();
        $date=$comd->getCreatedAt()->format('ymd');
		//dd($date);
        $cmd=$comd->getId();
        $user=$comd->getUtilisateur();
        $client= $em->getRepository('UserBundle:User')->findOneBy(array('id' =>$user));
        $numro=$client->getNumero();
        //dd($name);
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
         // Provide a name for your file with extension
        $filename = 'TextFile'.$num.'.txt';
        
     
        $fileContent = " hello";
     
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
		
		
		
		 $content = $this->render('commandes/test.html.twig', array(
		 'comd'=>$comd,
		 'items'=>$items,
		 'produits'=>$produits,
		 'products'=>$products,
		 'client'=>$client,
		 'date'=>$date
		 
		 ));
	
        $textResponse = new Response($content , 200);
        $textResponse->headers->set('Content-Type', 'text/plain');
		 $dispositionHeader = $textResponse->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $textResponse->headers->set('Content-Disposition', $dispositionHeader);
		//file_put_contents('test.txt',$textResponse);
		
        return $textResponse;

        // Dispatch request
       // return $response;

    }

    /**
     * Creates a new commande entity.
     *
     */
    public function newAction(Request $request)
    {
        $commande = new Commande();
        $form = $this->createForm('CommandeBundle\Form\CommandesType', $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush();

            return $this->redirectToRoute('commandes_show', array('id' => $commande->getId()));
        }

        return $this->render('commandes/new.html.twig', array(
            'commande' => $commande,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function showAction(Commandes $commande)
    {
        $deleteForm = $this->createDeleteForm($commande);

        return $this->render('commandes/show.html.twig', array(
            'commande' => $commande,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing commande entity.
     *
     */
    public function vldAction(Request $request, Commandes $commande)
    {
        $em = $this->getDoctrine()->getManager();
        $comd= $em->getRepository('CommandeBundle:Commandes')->findOneBy(array('id' =>$commande));

        $cmd=$comd->getId();
        $items= $em->getRepository('CommandeBundle:OrderItems')->findByOrder( array('order'=>$cmd));
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
        $editForm = $this->createForm('CommandeBundle\Form\Commandes2Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-en-traitement', array('id' => $commande->getId()));
        }

        return $this->render('CommandeBundle:espace-commande:vld-comd.html.twig', array(
            'commande' => $commande,
            'produits' => $produits,
            'products' => $products,
            'items' => $items,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * Displays a form to edit an existing commande entity.
     *
     */
    public function factAction(Request $request, Commandes $commande)
    {
        $em = $this->getDoctrine()->getManager();
        $comd= $em->getRepository('CommandeBundle:Commandes')->findOneBy(array('id' =>$commande));

        $cmd=$comd->getId();
        $items= $em->getRepository('CommandeBundle:OrderItems')->findByOrder( array('order'=>$cmd));
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
        $editForm = $this->createForm('CommandeBundle\Form\Commandes3Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-termine', array('id' => $commande->getId()));
        }

        return $this->render('CommandeBundle:espace-commande:facture.html.twig', array(
            'commande' => $commande,
            'produits' => $produits,
            'products' => $products,
            'items' => $items,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * Displays a form to edit an existing commande entity.
     *
     */
    public function editAction(Request $request, Commandes $commande)
    {
        $em = $this->getDoctrine()->getManager();
        $comd= $em->getRepository('CommandeBundle:Commandes')->findOneBy(array('id' =>$commande));

        $cmd=$comd->getId();
        $items= $em->getRepository('CommandeBundle:OrderItems')->findByOrder( array('order'=>$cmd));
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
        $editForm = $this->createForm('CommandeBundle\Form\Commandes1Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-recu', array('id' => $commande->getId()));
        }

        return $this->render('commandes/edit.html.twig', array(
            'commande' => $commande,
            'produits' => $produits,
            'products' => $products,
            'items' => $items,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a commande entity.
     *
     */
    public function deleteAction(Request $request, Commandes $commande)
    {
        $form = $this->createDeleteForm($commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commande);
            $em->flush();
        }

        return $this->redirectToRoute('comd-recu');
    }

    /**
     * Creates a form to delete a commande entity.
     *
     * @param Commandes $commande The commande entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Commandes $commande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commandes_delete', array('id' => $commande->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
