<?php

namespace CommandeBundle\Controller;

use CommandeBundle\Entity\Categorie;
use CommandeBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CommandeBundle\Entity\Commandes;
use CommandeBundle\Entity\OrderItems;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class DefaultController extends Controller
{
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAction()
    {
        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {


            return $this->render('CommandeBundle:espace-commande:index.html.twig');

        }
        else {
            return $this->render('homepage');
        }
    }
    public function catAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       $category = $em->getRepository('CommandeBundle:Category')->findAll();
        $categorie = $em->getRepository('CommandeBundle:Categorie')->findBy(array('category'=>$category));
        return $this->render('CommandeBundle:Default:category.html.twig',array(
            'category' => $category,
            'categorie' => $categorie

        ));
    }
    /**
     * Finds and displays a commande entity.
     *
     */
    public function subcatAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository('CommandeBundle:Categorie')->findBy(array('category'=>$category));
        return $this->render('CommandeBundle:Default:subcat.html.twig', array(
            'categorie' => $categorie,
            'category' => $category
        ));
    }
    public function nvcmdAction(Categorie $categorie, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $produits= $em->getRepository('CommandeBundle:Produit')->findBy(array('categorie'=>$categorie));
        $products= $em->getRepository('CommandeBundle:Prixprod')->findBy(
            array('utilisateur' => $utilisateur),array('id' => 'DESC')
        );


        //dd($pagination);

//dd($products);
        return $this->render('CommandeBundle:Default:nvcommande.html.twig',array(
           'produits' => $produits,
            'categorie'=>$categorie,
            'products' => $products,
            'utilisateur' => $utilisateur,



        ));

    }

    public function resultAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $data = $request->get("cart_list");
        $data = json_decode($data,true);
        $random = random_int(0001,9999);
        //dd($data);
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $order = new Commandes();
        $order->setUtilisateur($utilisateur);
        $order->setNum($random);
        $order->setEtat(false);
        $order->setFin(false);
       // dd($order);
        $em->persist($order);
        $em->flush();
        $message = \Swift_Message::newInstance()
            ->setSubject('nouvelle commande')
            ->setFrom('contact.chayuan@gmail.com')
            ->setTo('salwawebdev@gmail.com')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'CommandeBundle:Default:mail.html.twig',
                    array('name' => $utilisateur->getUsername())
                ),
                'text/html'
            )

        ;
        $this->get('mailer')->send($message);

        $contact = \Swift_Message::newInstance()
            ->setSubject('Nouvelle commande')
            ->setFrom('contact.chayuan@gmail.com')
            ->setTo('salwa23hleli@gmail.com')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'CommandeBundle:Default:mailadmin.html.twig',
                    array('name' => $utilisateur->getUsername())
                ),
                'text/html'
            )

        ;
        $this->get('mailer')->send($contact);
        foreach ($data as $product) {
            $qty = floatval($product['product_quantity']);

            $price = floatval($product['product_price']);
            $orderItem = new OrderItems();
            $orderItem->setOrder($order->getId());
            $orderItem->setProduct(intval($product['product_id']));
            $orderItem->setQty($qty);
            $orderItem->setPrice($price);
            $orderItem->setTotal($qty * $price);

            $em->persist($orderItem);
        }
        $this->addFlash("success", "This is a success message");
        $em->flush();
        return $this->redirectToRoute('liste_cmd');
    }

    public function listcmdAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $commandes = $em->getRepository('CommandeBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur),array('id' => 'DESC')
        );

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $commandes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit',3)
        /*limit per page*/
        );
       //dd($pagination);

        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findBy(
            array('utilisateur' => $utilisateur)
        );
//dd($products);
        return $this->render('CommandeBundle:Default:liste-commande.html.twig',array(
            'produits' => $produits,
            'products' => $products,
            'utilisateur' => $utilisateur,
             'items' => $items,
            'commandes' => $commandes,
            'pagination' => $pagination


        ));

    }
    public function listfactAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $commandes = $em->getRepository('CommandeBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur ,'fin'=> 1)
            ,array('id' => 'DESC')
        );
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $commandes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit',1)
        /*limit per page*/
        );
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findBy(
            array('utilisateur' => $utilisateur)
        );
//dd($products);
        return $this->render('CommandeBundle:Default:liste-facture.html.twig',array(
            'produits' => $produits,
            'products' => $products,
            'utilisateur' => $utilisateur,
            'items' => $items,
            'commandes' => $commandes,
            'pagination' => $pagination


        ));

    }
    public function comdrecuAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('CommandeBundle:Commandes')->findBy(
            array('etat'=>0, 'fin'=>0)
            ,array('id' => 'DESC')
        );
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $commandes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit',3)
        /*limit per page*/
        );
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
//dd($products);
        return $this->render('CommandeBundle:espace-commande:commandes-recus.html.twig',array(
            'produits' => $produits,
            'products' => $products,

             'items' => $items,
            'commandes' => $commandes,
            'pagination' => $pagination


        ));

    }
    public function traitementAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('CommandeBundle:Commandes')->findBy(
            array('etat'=>1, 'fin'=>0) ,array('id' => 'DESC')
        );
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $commandes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit',2)
        /*limit per page*/
        );
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
//dd($products);
        return $this->render('CommandeBundle:espace-commande:comd-en-traitement.html.twig',array(
            'produits' => $produits,
            'products' => $products,

            'items' => $items,
            'commandes' => $commandes,
            'pagination' => $pagination


        ));

    }
    public function fincomdAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('CommandeBundle:Commandes')->findBy(
            array('etat'=>1, 'fin'=>1) ,array('id' => 'DESC')
        );
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $commandes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit',1)
        /*limit per page*/
        );
        $items= $em->getRepository('CommandeBundle:OrderItems')->findAll();
        $produits= $em->getRepository('CommandeBundle:Produit')->findAll();
        $products= $em->getRepository('CommandeBundle:Prixprod')->findAll();
//dd($products);
        return $this->render('CommandeBundle:espace-commande:comd-termine.html.twig',array(
            'produits' => $produits,
            'products' => $products,

            'items' => $items,
            'commandes' => $commandes,
            'pagination' => $pagination


        ));

    }


}
