<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class DefaultController extends Controller
{
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAction()
    {
       if (true == $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {


            return $this->render('UserBundle:Default:profile.html.twig');

        }
        else {
            return $this->render('homepage');
        }

    }
}
